



#' Restructure cnr data list to a data frame
#'
#' @param data.cnr
#'
#' @return
#' @export
#'
#' @examples
reformatCNRextraction <- function(data.cnr){
  data.cnr.list <- lapply(names(data.cnr), function(fishery){
    data.fishery <- data.cnr[[fishery]]


    data.tmp <- lapply(names(data.fishery), function(year, fishery){
    	#if(fishery=="fishery.model25" & year=="year2003") browser()
      data.subset <- data.fishery[[year]][names(data.fishery[[year]]) !="comment" & !is.na(names(data.fishery[[year]]))]
      if(!"cnrmethod" %in% names(data.subset)) data.subset$cnrmethod <- NA
      df <- data.frame(fisherymodel49=fishery, year= as.integer(gsub("year", "", year)), cnrfishery=data.subset$cnrfishery, cnrmethod=data.subset$cnrmethod, variable=names(data.subset), value=unlist(data.subset))
      #remove cnrfishery & cnrmethod from variable column as they have their own columns
      df[! df$variable %in% c( "cnrmethod"),]
    }, fishery)

    do.call(rbind, data.tmp)
  })

  data.tmp <- do.call(rbind, data.cnr.list)
  row.names(data.tmp) <- NULL


  data.tmp$fisherymodel49 <- type.convert(gsub("fishery.model", "", data.tmp$fisherymodel49), as.is = TRUE)
  data.tmp <- data.tmp[data.tmp$variable %in% c('catchlanded', 'encounterslegal', 'encounterssublegal', 'seasonlengthcnr', 'seasonlengthlegal'),]

  data.tmp[!is.na(data.tmp$fisherymodel49),]

}#END reformatCNRextraction
