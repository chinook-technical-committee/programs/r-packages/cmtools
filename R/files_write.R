

#' Write .cnr file
#'
#' @param x
#' @param filename
#'
#' @return
#' @export
#'
#' @examples
writeCNRfile <- function(x, filename="CLBxxxx.CNR"){

invisible({
text <- paste(x[1:2], c("START YEAR", "END YEAR"), sep = ", ")
	writeLines(text, filename)

	lapply(x$data.fishery, function(data.fishery){

		variable.order <- c('cnrfishery', 'cnrmethod', 'encounterslegal', 'encounterssublegal', 'catchlanded', 'seasonlengthlegal', 'seasonlengthcnr', 'comment')
		data.fishery[['data.cnr']] <- lapply(data.fishery[['data.cnr']], function(x){
					x[order(match(names(x), variable.order))]
		})

		#need to add in words 'fishery index' as that is a search term in the readcnr func
		data.fishery$fishery$fisherymodelnamelong <- paste0("FISHERY INDEX - ", data.fishery$fishery$fisherymodelnamelong)

				data.fishery <- c(data.fishery['fishery'], data.fishery['selectivity'], data.fishery[['data.cnr']])

		lapply(data.fishery, function(x.row){
			cat(unlist(x.row), sep = ", ", file = filename, append = TRUE)
			cat("\n", file = filename, append = TRUE)
		})
	})
})

}#END writeCNRfile



writeCTRLfiles <- function(ctrl.list=cmtools::defs$ctrl.list, folder="."){

	ctrl.list2 <- lapply(names(ctrl.list) , function(listname) {
		op7.filename <- tools::file_path_sans_ext(listname)
		ctrl.list[[listname]]$op7$value <- paste(folder, op7.filename, sep="/")
		apply(matrix(unlist(ctrl.list[[listname]]), ncol = 2, byrow = TRUE), 1, paste, collapse=",")
	})

	names(ctrl.list2) <- names(ctrl.list)
	invisible(
		lapply(names(ctrl.list2), function(filename){

			filepath <- paste(folder, filename, sep="/")
			ctctools::writeListToText(ctrl.list2[[filename]], filename = filepath, overwrite = TRUE)
		})
	)
}#END writeCTRLfiles





writeCTRLfiles.old <- function(ctrl.list=cmtools::defs$ctrl.list, folder=".", op7.filenames=list("A.OP7", "B.OP7", "P.OP7")){

	ctrl.list2 <- lapply(seq_along(ctrl.list) , function(i) {
		ctrl.list[[i]]$op7$value <- paste(folder, op7.filenames[[i]], sep="/")
		apply(matrix(unlist(ctrl.list[[i]]), ncol = 2, byrow = TRUE), 1, paste, collapse=",")
	})

	names(ctrl.list2) <- paste0(unlist(op7.filenames), ".ctrl")
	invisible(
		lapply(names(ctrl.list2), function(filename){

			filepath <- paste(folder, filename, sep="/")
			ctctools::writeListToText(ctrl.list2[[filename]], filename = filepath, overwrite = TRUE)
		})
	)
}#END writeCTRLfiles.old

# writeCTRLfiles <- function(folder=".", op7.filenames){
#
# 	output <- list(
# 		a=paste0("
# # here is a comment blank lines are not currently allowed
# # for now we need to include the full path to the op7 file
# op7,", paste(folder, op7.filenames$OP7a, sep="/" ),
# "\n# second comment
# Convtolerance, 0.05
# StkSclrMin, 0.01
# NumIter, 100"),
#
# b=paste0("
# # here is a comment blank lines are not currently allowed
# # for now we need to include the full path to the op7 file
# op7,", paste(folder, op7.filenames$OP7b, sep="/" ),
# "\n# second comment
# Convtolerance, 0.05
# StkSclrMin, 0.01
# NumIter, 100"),
#
# p=paste0("
# # here is a comment blank lines are not currently allowed
# # for now we need to include the full path to the op7 file
# op7,", paste(folder, op7.filenames$OP7p, sep="/" ),
# "\n# second comment
# Convtolerance, 0.05
# StkSclrMin, 0.01
# NumIter, 100
# optionCCC, True
# optionTotalMort, True
# optionISBM, True")
# 	)
#
# 	names(output) <- paste0(op7.filenames, ".ctrl")
# 	invisible(lapply(names(output), function(filename) {
# 		filepath <- paste(folder, filename, sep="/")
# 		ctctools::writeListToText(output[[filename]], filepath, overwrite = TRUE)}  ))
#
# }#END writeCTRLfiles




#' @title Write templates of three op7 control (.ctrl) files for chinook model
#'   in batch mode
#'
#' @param x A list
#'
#' @details These three op7 .ctrl files include the path to each op7 file, and
#' as such will need updating to the user-specific path.
#'
#'
#' @return Nothing
#' @export
#'
#' @examples
#' \dontrun{
#' #three .ctrl files will be written to the current working directory:
#' OpModel::writeListtext(ctrl.list)
#' }
writeListtext <- function(x){
invisible(
	lapply(names(x), function(x.name){
		writeLines(gsub("\\r", "", x[[x.name]]), con = x.name)
	})
)
}#END writeListtext




#' Write a list to a text file (moved to ctctools)
#'
#' @param x A list
#' @param filename Character length one. The name of file to export to.
#' @param overwrite Logical. Default is FALSE. If FALSE, then a warning is given
#'   if file already exists. If TRUE, then no warning is given and file will be
#'   over-written.
#' @param ... Arguments to be passed to the formatC function. For example
#'   format="E" and digits=8.
#'
#' @description
#' This function has been moved to ctctools.
#'
#'
#' @return
#' @export
#'
writeListToText <- function(x, filename="temp.txt", overwrite=FALSE, ...){

	cat("\nthis function has been moved to the ctctools package.\nconsider revising code to: ctctools::writeListToText()\n")
	ctctools::writeListToText(x, filename=filename, overwrite=overwrite, ...)

}#END writeListToText



#' Deprecated. Use writePNVfile()
#'
#' @param ...
#'
#' @return
#' @export
#'
#' @examples
writePNV <- function(...){
	writePNVfile(...)

}



#' Write .pnv file.
#'
#' @param x A list. Has same structure as output from \code{\link{readPNV}}. See details.
#' @param path Character vector. Location to write pnv files. Default is current
#'   working directory
#'
#' @details
#' The function argument x is a list. Each element of x is also a list and represents data in one pnv file (all the data for a single model fishery. The model fishery element does not need to have a name. Within the model fishery list there must be at least the following named elements (same as found in the PNV file):
#' * fishery.model.name (this will play a part in the pnv file name)
#' * fishery.model (an integer the model fishery number)
#' * year.start (four digits)
#' * year.end (four digits)
#' * data.pnv.long (data frame with three columns: year, pnv, age). age is an integer
#'
#'
#' @md
#' @return
#' @export
#'
#' @examples
writePNVfile <- function(x, path="."){
	invisible({
		lapply(x, function(data.fishery){

			filename <- data.fishery$fisherymodel49nameshort
			filename <- paste0(filename, "CLB.PNV")
			filename <- paste(path, filename, sep="/")

			data.pnv <- reshape(data.fishery$data.pnv.long[,c("year", "pnv", "age")], direction = 'wide', idvar = 'age', timevar = 'year')

			cols.order <- sort(colnames(data.pnv))
			data.pnv <- data.pnv[order(data.pnv$age),cols.order]
			data.pnv <- subset(data.pnv, select = -age)

			# forces 4 digits and tailing zero if needed:
			data.pnv <- apply(data.pnv, 2, function(x)sprintf("%.4f", x))

			#put everything to be written into a list. each list element is a line to write.
			list.tmp <- c(data.fishery[c("fishery.model","year.start","year.end" )], split(data.pnv, seq(nrow(data.pnv))))

			file.create(filename)

			lapply(list.tmp, function(x){
				cat(format(unlist(x),sci=F), file = filename, append = TRUE)
				cat("\n", file = filename, append = TRUE)
			})

		})
	})
}#END writePNVfile


