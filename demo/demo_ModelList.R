
####### SETUP #######
rm(list=ls())
###### COMMENTS ########
script.name <- 'ModelListBuilder'
if(!exists('script.metadata')) script.metadata <- list()

script.metadata[[script.name]] <- list(
fileName = paste0(script.name,'.R'),
author= 'Michael Folkes',
date.creation='2021-03-09 19:04:22',
date.edit= NA,
comments = NA
)# END list


commonstocks <- FALSE
stock.names <- 'all'
#stock.names <- c('urb','sum', 'spr', 'mcb')

stockmap.pathname <- '../data/Old-New_Stock_Mapping_20160916.csv'
#stockmap.pathname <- NA

data.path.vec <- c('../data/calibration2017/CLB17bb', '../data/calibration2017/CLB17cc')
#data.path.vec <- c('../data/2016_CI_test')

stocks.key.pathname.vec <- c('../data/calibration2017/CLB17bb/stocks.oldnames.csv', '../data/calibration2017/CLB17cc/stocks.oldnames.csv')

# this can be 'brood.year' or 'return.year':
grouping.year <- 'return.year'

age.structure <- TRUE
totalabundance <- TRUE
data.type <- c('escapement', 'terminalrun')

samplesize.min <- 10
results.path <- '../data/calibration2017'

ranking <- c('ordinal', 'interpolated')

doPlots <- TRUE  # TRUE OR FALSE. ALL CAPS

savepng <-  TRUE  # TRUE OR FALSE. ALL CAPS

model.list <- buildmodel.list(commonstocks = commonstocks, stockmap.pathname = stockmap.pathname, data.path.vec = data.path.vec, stocks.key.pathname.vec = stocks.key.pathname.vec, grouping.year = grouping.year, age.structure = age.structure, totalabundance =totalabundance, data.type=data.type, results.path = results.path, stock.names = stock.names, groupingby=c( 'agegroup'), ranking = ranking)


####### END #######


